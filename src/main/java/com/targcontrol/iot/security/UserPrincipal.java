package com.targcontrol.iot.security;

import com.targcontrol.iot.models.User;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@AllArgsConstructor
@Getter
public class UserPrincipal implements UserDetails {

  private UUID id;
  private String username;
  private String password;
  private boolean enable;

  private Collection<? extends GrantedAuthority> authorities;

  public static UserPrincipal build(User user) {
    List<GrantedAuthority> authorities = List.of(new SimpleGrantedAuthority(user.getRole().name()));

    return new UserPrincipal(
        user.getId(),
        user.getUsername(),
        user.getPassword(),
        user.isEnable(),
        authorities
    );
  }


  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return enable;
  }
}