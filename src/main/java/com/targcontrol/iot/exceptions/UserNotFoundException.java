package com.targcontrol.iot.exceptions;

import java.util.UUID;

public class UserNotFoundException extends RuntimeException {
    private final static String USER_NOT_FOUND_EXCEPTION_MESSAGE = "User with specified Id = %s not found";

    public UserNotFoundException(UUID id) {
        super(String.format(USER_NOT_FOUND_EXCEPTION_MESSAGE, id.toString()));
    }
}
