package com.targcontrol.iot.models;

public enum DeviceState {
  DEVICE_ACTIVE, DEVICE_DELETED
}
