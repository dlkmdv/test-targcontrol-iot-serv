package com.targcontrol.iot.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Table(name = SensorData.TABLE_NAME, schema = SensorData.SCHEMA_NAME)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SensorData {

    public static final String SCHEMA_NAME = "targcontrol_iot";
    public static final String TABLE_NAME = "sensor_data";

    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "organization_id")
    private UUID organizationId;

    @Column(name = "device_id")
    private UUID deviceId;

    @Column(name = "sensor_id")
    private UUID sensorId;

    @Column(name = "date_time")
    private ZonedDateTime dateTime;

    @Column(name = "value")
    private Double value;
}
