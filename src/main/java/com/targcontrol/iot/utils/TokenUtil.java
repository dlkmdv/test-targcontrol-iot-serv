package com.targcontrol.iot.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TokenUtil {

  //TODO
  private static final UUID ORG_ID_TEMP = UUID.fromString("8f8e70ec-eb38-461b-807c-5133ec637f2f");

  public UUID getAuthOrgId() {
    return ORG_ID_TEMP;
  }


}
