package com.targcontrol.iot.views;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.ZonedDateTime;
import java.util.UUID;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SensorDataRequest {

  @NotNull
  private UUID organizationId;

  @NotNull
  private ZonedDateTime startDate;

  @NotNull
  private ZonedDateTime endDate;

  @JsonIgnore
  @AssertTrue
  public boolean isIntervalValidCheck() {
    return this.startDate != null && this.endDate != null && (this.startDate.isBefore(this.endDate)
        || this.startDate.isEqual(this.endDate));
  }
}
