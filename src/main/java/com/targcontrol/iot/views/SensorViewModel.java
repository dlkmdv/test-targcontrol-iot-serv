package com.targcontrol.iot.views;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class SensorViewModel {

  @NotNull
  private UUID id;

  @NotNull
  private String name;
}
