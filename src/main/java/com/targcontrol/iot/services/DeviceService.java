package com.targcontrol.iot.services;

import com.targcontrol.iot.exceptions.DeviceNotFoundException;
import com.targcontrol.iot.models.Device;
import com.targcontrol.iot.models.DeviceState;
import com.targcontrol.iot.repositories.DeviceRepository;
import com.targcontrol.iot.utils.TokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DeviceService {

  private final TokenUtil tokenUtil;
  private final DeviceRepository deviceRepository;

  public Device findByIdAndOrg(UUID id, UUID organizationId) {
    Optional<Device> device = deviceRepository.findByIdAndOrganizationId(id, organizationId);
    if (device.isEmpty()) {
      throw new DeviceNotFoundException(id);
    }
    return device.get();
  }

  public List<Device> findAllByOrg(UUID organizationId) {
    return this.deviceRepository.findAllByOrganizationId(organizationId);
  }

  public Device create(Device device) {
    UUID orgId = tokenUtil.getAuthOrgId();
    device.setOrganizationId(orgId);
    device.setToken(Device.getNewDeviceToken());
    device.setDeviceState(DeviceState.DEVICE_ACTIVE);
    return this.deviceRepository.save(device);
  }

  public Device update(Device device) {
    UUID orgId = tokenUtil.getAuthOrgId();
    device.setOrganizationId(orgId);
    if (deviceRepository.findByIdAndOrganizationId(device.getId(), orgId).isEmpty()) {
      throw new DeviceNotFoundException(device.getId());
    }
    return this.deviceRepository.save(device);
  }

  public void delete(Device device) {
    UUID orgId = tokenUtil.getAuthOrgId();
    if (deviceRepository.findByIdAndOrganizationId(device.getId(), orgId).isEmpty()) {
      throw new DeviceNotFoundException(device.getId());
    }
    device.setDeviceState(DeviceState.DEVICE_DELETED);
    this.deviceRepository.save(device);
  }
}
