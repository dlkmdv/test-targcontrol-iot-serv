package com.targcontrol.iot.services;

import com.targcontrol.iot.exceptions.DuplicatedEntryException;
import com.targcontrol.iot.models.User;
import com.targcontrol.iot.repositories.UserRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;

  public List<User> findAll() {
    return userRepository.findAll();
  }

  public Optional<User> findById(UUID id) {
    return userRepository.findById(id);
  }

  public User save(User user) {
    if (userRepository.findByUsername(user.getUsername()).isPresent()) {
      throw new DuplicatedEntryException("username", user.getUsername(), User.class);
    }

    if (userRepository.findByEmail(user.getEmail()).isPresent()) {
      throw new DuplicatedEntryException("email", user.getEmail(), User.class);
    }

    user.setPassword(passwordEncoder.encode(user.getPassword()));
    return userRepository.save(user);
  }

  public User update(User user) {
    user.setPassword(passwordEncoder.encode(user.getPassword()));

    return userRepository.save(user);
  }

  public void delete(User user) {
    userRepository.delete(user);
  }
}
