CREATE TABLE targcontrol_iot.device (
  id                UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
  organization_id   UUID NOT NULL,
  name              TEXT NOT NULL,
  token             TEXT NOT NULL UNIQUE,
  device_state      TEXT NOT NULL
 );
