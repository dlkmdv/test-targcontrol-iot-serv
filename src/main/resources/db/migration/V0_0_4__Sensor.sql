CREATE TABLE targcontrol_iot.sensor
(
    id              UUID DEFAULT uuid_generate_v4() PRIMARY KEY,
    organization_id UUID NOT NULL,
    name            TEXT NOT NULL,
    device_id       UUID REFERENCES targcontrol_iot.device (id) ON DELETE CASCADE
);