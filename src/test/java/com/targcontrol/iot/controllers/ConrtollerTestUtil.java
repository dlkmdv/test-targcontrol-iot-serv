package com.targcontrol.iot.controllers;

import com.targcontrol.iot.models.DeviceState;
import com.targcontrol.iot.views.DeviceRequest;
import java.util.Optional;
import java.util.UUID;

public class ConrtollerTestUtil {

  public DeviceRequest create(Optional<UUID> id, String name, DeviceState state,
      Optional<String> token) {
    DeviceRequest result = new DeviceRequest();
    id.ifPresent(result::setId);
    result.setName(name);
    result.setDeviceState(state);
    token.ifPresent(result::setToken);
    return result;
  }
}
