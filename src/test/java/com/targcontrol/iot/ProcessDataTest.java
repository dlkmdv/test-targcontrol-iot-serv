package com.targcontrol.iot;

import com.targcontrol.iot.models.SensorData;
import lombok.val;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class ProcessDataTest {


    @Test
    public void whenProcess_thenOK() {
        double threshold = 1000.0;
        double min = 0.0;
        double max = 1.0;

        ZonedDateTime st = ZonedDateTime.now();
        List<SensorData> data = Arrays.asList(
                SensorData.builder().value(389.0).dateTime(st).build(),
                SensorData.builder().value(392.0).dateTime(st.plusDays(1)).build(),
                SensorData.builder().value(400.0).dateTime(st.plusDays(2)).build(),

                SensorData.builder().value(2047.0).dateTime(st.plusDays(3)).build(),
                SensorData.builder().value(2047.0).dateTime(st.plusDays(4)).build(),

                SensorData.builder().value(372.0).dateTime(st.plusDays(5)).build(),
                SensorData.builder().value(407.0).dateTime(st.plusDays(6)).build()

        );

        if (data.size() < 2) {
            return;
        }

        //threshold filter
        data = data.parallelStream()
                .peek(x -> x.setValue((x.getValue() > threshold) ? max : min))
                .collect(Collectors.toList());


        List<SensorData> result = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {

            if (i == 0) {
                result.add(data.get(i));
                continue;
            }

            if (i == data.size() - 1) {
                result.add(data.get(data.size() - 1));
                continue;
            }

            double current = data.get(i).getValue();
            double next = data.get(i + 1).getValue();

            if (current != next) {
                result.add(data.get(i));
                result.add(data.get(i + 1));
            }
        }

        result.stream().forEach(System.out::println);

    }

}
