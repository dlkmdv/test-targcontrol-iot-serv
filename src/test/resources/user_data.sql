insert into targcontrol_iot.user (id, username, pass, email, organization_id, role, enable)
values ('b26ba302-db7e-476f-bfc8-c93e786250b0', 'auth_username',
        '$2a$10$qhzLp8Eyyr7iiRPy/PSOiOc9SuwBl7l5rHdc0CTcs8n1DIc4Srupu', 'auth.test@gmail.com',
        '99b75e3a-5290-4d88-8f99-75a6560565b1', 'ROLE_USER', true);
insert into targcontrol_iot.user (id, username, pass, email, organization_id, role, enable)
values ('e8743415-0b41-4ad4-a100-d9e788b08348', 'auth_admin_username',
        '$2a$10$qhzLp8Eyyr7iiRPy/PSOiOc9SuwBl7l5rHdc0CTcs8n1DIc4Srupu', 'auth.admin.test@gmail.com',
        '3143c91b-ec88-4b4c-ae23-1ba56d324336', 'ROLE_ADMIN', true)